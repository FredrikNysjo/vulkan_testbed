# Third-Party Dependencies

Third-party dependencies for Vulkan testbed

## License

GLFW, GLM, imgui, Vulkan Memory Allocator (VMA), and stb_image are distributed under their own respective licensens. See `LICENSES.md` for details.
