# vulkan_testbed

Simple [Vulkan](https://www.vulkan.org) testbed used for testing stuff and to provide some abstractions over the API in personal projects.


## Example screenshots

![Screenshot](/screenshot.png?raw=true "Screenshot")


## Build instructions for Linux

You need to install the following tools, which should be available in the distributions package manager:

    gcc/g++, CMake 3.10 (or higher), make, git

In addition, you also need the [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/) (version 1.3 or later recommended), and a few other libraries from the package manager. On for example Ubuntu 18.04 and 20.04, you can install them by

    sudo apt install libxmu-dev libxi-dev libxrandr-dev libxcursor-dev libxinerama-dev

Once the tools and dependencies are installed, set the environment variable `VKTEST_ROOT` to the path where this `README.md` is located, and run the provided `build.sh` script to build and run the example application.


## Build instructions for Windows

On Windows, you can install VSCode and the Microsoft Visual C++ (MSVC) compiler via the instructions from https://code.visualstudio.com/docs/cpp/config-msvc , and then use CMake from the command line or via the [VSCode Tools addon](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) to generate and build the example application. You also need to install the [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/) (version 1.3 or later recommended).

The provided `build.sh` script can also be used from Git Bash on Windows, provided that you have CMake and the MSVC compiler installed. To change the CMake generator to match the MSVC version, you can use the `VKTEST_GENERATOR` environment variable (by default, this variable is set to "Visual Studio 16 2019" in the script).


## Usage

Running the script `build.sh` (from the terminal on Linux, or Git Bash on Windows) will automatically start the application after the build has finished. 

In `main.cpp`, there are a few constants that can be changed in the code to toggle between different scenes:

- `SHOW_TRIANGLE_SCENE`: A single triangle rendered with per-vertex colors.
- `SHOW_MESH_SCENE`: An mesh rendered with face normals and normal visualization. You can drag-and-drop an OBJ-file on the window to change the mesh being displayed. The OBJ-reader only supports meshes with vertex positions and no other attributes. There is also a virtual trackball to control the camera with the mouse. 
- `SHOW_BILLBOARD_SCENE`: A textured billboard. You can drag-and-drop a PNG-image on the window to change the texture being displayed.


## Third-party dependencies

The following third-party libraries are included in the `third-party` folder and compiled from source during the build:

- GLFW v3.3.2 (https://github.com/glfw/glfw)
- GLM v0.9.9.8 (https://github.com/g-truc/glm)
- ImGui v1.79 (https://github.com/ocornut/imgui)
- Vulkan Memory Allocator (VMA) v3.0.1 (https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator)
- stb_image.h v2.26 (https://github.com/nothings/stb)


## Code style

This code uses the WebKit C++ style (with minor modifications) and clang-format (version 6.0) for automatic formatting.


## License

The code is provided under the MIT license (see LICENSE.md).
