#!/bin/bash

if [ -z "$VKTEST_ROOT" ]; then
    echo "VKTEST_ROOT is not set"
    exit 1
fi

if [ -z "$VKTEST_GENERATOR" ]; then
    VKTEST_GENERATOR="Unix Makefiles"
    if [[ "$OSTYPE" == "msys" ]]; then
        VKTEST_GENERATOR="Visual Studio 16 2019"
    fi
fi

# Generate build files
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -G "$VKTEST_GENERATOR" ../ -DCMAKE_INSTALL_PREFIX=../ && \

# Build and install the program
cmake --build . --config RelWithDebInfo --target install && \

# Run the program
cd ../bin && \
`find . -executable -type f`
