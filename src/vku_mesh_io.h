/// @file
/// @brief Data structures and functions for mesh I/O
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include <vector>
#include <stdint.h>

namespace vku {

struct OBJMesh {
    std::vector<float> positions;
    std::vector<int32_t> indices;
};

// This function only provides very basic reading of OBJ-meshes that contain no
// extra vertex attributes such as normals or texture coordinates
void read_obj(const char *filename, OBJMesh *mesh);

}  // namespace vku
