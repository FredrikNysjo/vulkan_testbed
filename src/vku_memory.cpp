/// @file
/// @brief Enables definitions for Vulkan Memory Allocator (VMA) library
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"
