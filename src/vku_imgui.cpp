/// @file
/// @brief Helper functions for dealing with ImGui in Vulkan
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vku_imgui.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_vulkan.h>

#include <cstdlib>
#include <string>
#undef NDEBUG
#include <cassert>

#define LOG_INFO(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_DEBUG(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_ERROR(...) std::fprintf(stderr, __VA_ARGS__)

namespace vku {

void init_imgui(const VkuContext &vtx, const VkRenderPass renderPass, GLFWwindow *window,
                bool installCallbacks)
{
    LOG_INFO("vku::init_imgui: Initialising...\n");
    assert(vtx.device != VK_NULL_HANDLE);
    assert(vtx.queue != VK_NULL_HANDLE);
    assert(vtx.descriptorPool != VK_NULL_HANDLE);
    assert(renderPass != VK_NULL_HANDLE);
    assert(window != nullptr);

    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForVulkan(window, installCallbacks);
    ImGui_ImplVulkan_InitInfo info = {};
    info.Instance = vtx.instance;
    info.PhysicalDevice = vtx.physicalDevice;
    info.Device = vtx.device;
    info.Queue = vtx.queue;
    info.DescriptorPool = vtx.descriptorPool;
    info.MinImageCount = 2;
    info.ImageCount = 2;
    ImGui_ImplVulkan_Init(&info, renderPass);

    // Also need to create and upload a fonts texture
    {
        vkResetCommandPool(vtx.device, vtx.cmdPool, 0);
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        vkBeginCommandBuffer(vtx.cmdBuffers[vtx.swapIndex], &beginInfo);

        ImGui_ImplVulkan_CreateFontsTexture(vtx.cmdBuffers[vtx.swapIndex]);

        VkSubmitInfo endInfo = {};
        endInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        endInfo.commandBufferCount = 1;
        endInfo.pCommandBuffers = &vtx.cmdBuffers[vtx.swapIndex];
        vkEndCommandBuffer(vtx.cmdBuffers[vtx.swapIndex]);
        vkQueueSubmit(vtx.queue, 1, &endInfo, VK_NULL_HANDLE);

        vkDeviceWaitIdle(vtx.device);
        ImGui_ImplVulkan_DestroyFontUploadObjects();
    }

    LOG_INFO("vku::init_imgui: Done\n");
}

void shutdown_imgui(const VkuContext &vtx)
{
    LOG_INFO("vku::shutdown_imgui: Shutting down...\n");
    assert(vtx.device != VK_NULL_HANDLE);

    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    LOG_INFO("vku::shutdown_imgui: Done\n");
}

}  // namespace vku