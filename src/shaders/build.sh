#!/bin/bash

glslangValidator -V -o draw_triangle.vert.spv draw_triangle.vert
glslangValidator -V -o draw_triangle.frag.spv draw_triangle.frag
glslangValidator -V -o draw_mesh.vert.spv draw_mesh.vert
glslangValidator -V -o draw_mesh.frag.spv draw_mesh.frag
glslangValidator -V -o draw_billboard.vert.spv draw_billboard.vert
glslangValidator -V -o draw_billboard.frag.spv draw_billboard.frag
