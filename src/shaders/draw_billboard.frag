#version 450

layout(set = 0, binding = 0) uniform sampler2D u_texture;

layout(location = 0) in vec2 in_texCoord;

layout(location = 0) out vec4 out_color;

void main()
{
    vec4 texel = texture(u_texture, vec2(in_texCoord.x, 1.0 - in_texCoord.y));
    out_color = vec4(texel.rgb * texel.a, 1.0);
}
