#version 450

layout(set = 0, binding = 0, std430) restrict buffer readonly VertexBuffer {
    vec4 data[];
} u_vertices;

layout(set = 0, binding = 1, std140) uniform SceneUniforms {
    mat4 projFromView;
    mat4 viewFromWorld;
    mat4 worldFromLocal;
} u_scene;

layout(location = 0) in vec4 in_localPos;

layout(location = 0) out vec3 out_viewPos;

void main()
{
    vec4 worldPos = u_scene.worldFromLocal * in_localPos;
    vec4 viewPos = u_scene.viewFromWorld * worldPos;

    out_viewPos = vec3(viewPos);
    gl_Position = u_scene.projFromView * viewPos;
    // Map NDC depth from range [-1, 1] to Vulkan range [0, 1]
    gl_Position.z = (gl_Position.z + gl_Position.w) / 2.0;
}
