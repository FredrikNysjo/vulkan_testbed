#version 450

layout(location = 0) out vec3 out_color;

void main()
{
    // Generate vertex colors for the triangle
    out_color = vec3(0.0); out_color[gl_VertexIndex % 3] = 1.0;

    gl_Position = vec4(vec2(gl_VertexIndex % 2, gl_VertexIndex / 2) - 0.5, 0.0, 1.0);
    gl_Position.x += (gl_VertexIndex / 2) * 0.5;
    gl_Position.y = -gl_Position.y;
}
