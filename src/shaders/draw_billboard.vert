#version 450

layout(location = 0) out vec2 out_texCoord;

void main()
{
    out_texCoord = vec2(gl_VertexIndex % 2, gl_VertexIndex / 2);
    gl_Position = vec4(out_texCoord - 0.5, 0.0, 1.0);
}
