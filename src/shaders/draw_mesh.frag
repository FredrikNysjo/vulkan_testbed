#version 450

layout(location = 0) in vec3 in_viewPos;
layout(location = 0) out vec4 out_color;

vec3 calculate_flat_normal(vec3 viewPos)
{
    // Compute flat normal vector via per-pixel derivatives
    vec3 deltaPos1 = dFdx(viewPos);
    vec3 deltaPos2 = -dFdy(viewPos);  // OpenGL: dFdy(viewPos), Vulkan: -dFdy(viewPos)
    return normalize(cross(deltaPos1, deltaPos2));
}

void main()
{
    vec3 viewNormal = calculate_flat_normal(in_viewPos);
    out_color = vec4(viewNormal * 0.5 + 0.5, 1.0);
}
