/// @file
/// @brief Data types and utility functions for handling interaction
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms of the MIT
/// license. See the LICENSE.md file for details.
///

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace cg {

// Struct for representing state of a virtual trackball
struct Trackball {
    glm::vec2 center = glm::vec2(0.0f);
    glm::quat orient = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
    float speed = 0.004f;
    float clamp = 100.0f;
    bool tracking = false;
};

// Struct for representing state of a panning transform
struct Panning {
    glm::vec2 center = glm::vec2(0.0f);
    glm::vec3 translation = glm::vec3(0.0f);
    float speedXY = 0.0025f;
    float speedZ = 0.25f;
    bool panning = false;
};

// Update trackball state from 2D mouse position input
void trackball_move(Trackball &trackball, float x, float y,
                    const glm::mat4 &worldFromView = glm::mat4(1.0f));

// Update panning state from 2D mouse position input
void panning_move_xy(Panning &panning, float x, float y,
                     const glm::mat4 &worldFromView = glm::mat4(1.0f));

// Update panning state from 1D mouse scroll input
void panning_move_z(Panning &panning, float zOffset,
                    const glm::mat4 &worldFromView = glm::mat4(1.0f));

}  // namespace cg
