/// @file
/// @brief  Abstractions for the Vulkan API
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vku_core.h"

#include <cstdio>
#include <cstring>
#include <string>
#undef NDEBUG
#include <cassert>

#define LOG_INFO(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_DEBUG(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_ERROR(...) std::fprintf(stderr, __VA_ARGS__)

namespace vku {

static std::string get_api_string(uint32_t api_version)
{
    char s[80] = {};
    std::sprintf(s, "%d.%d", VK_VERSION_MAJOR(api_version), VK_VERSION_MINOR(api_version));
    return s;
}

static void debug_print_instance_info(const char *indent = "")
{
    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, &extensions[0]);
    LOG_DEBUG("%sExtension count (instance): %d\n", indent, extensionCount);
    for (uint32_t i = 0; i < extensionCount; ++i) {
        LOG_DEBUG("%s  %s\n", indent, extensions[i].extensionName);
    }
}

static void debug_print_device_info(VkPhysicalDevice device, const char *indent = "")
{
    VkPhysicalDeviceProperties deviceProps = {};
    vkGetPhysicalDeviceProperties(device, &deviceProps);
    LOG_DEBUG("%s%s\n", indent, deviceProps.deviceName);
    LOG_DEBUG("%s  Vulkan version: %s\n", indent, get_api_string(deviceProps.apiVersion).c_str());

    uint32_t extensionCount = 0;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, &extensions[0]);
    LOG_DEBUG("%s  Extension count (device): %d\n", indent, extensionCount);
    for (uint32_t i = 0; i < extensionCount; ++i) {
        LOG_DEBUG("%s    %s\n", indent, extensions[i].extensionName);
    }

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, &queueFamilies[0]);
    LOG_DEBUG("%s  Queue family count: %d\n", indent, queueFamilyCount);
    for (uint32_t i = 0; i < queueFamilyCount; ++i) {
        LOG_DEBUG("%s    Queue familiy index: %d\n", indent, i);
        LOG_DEBUG("%s      Queue count: %d\n", indent, queueFamilies[i].queueCount);
        LOG_DEBUG("%s      Queue flags: %s%s%s%s\n", indent,
                  ((queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) ? "GRAPHICS " : ""),
                  ((queueFamilies[i].queueFlags & VK_QUEUE_COMPUTE_BIT) ? "COMPUTE " : ""),
                  ((queueFamilies[i].queueFlags & VK_QUEUE_TRANSFER_BIT) ? "TRANSFER " : ""),
                  ((queueFamilies[i].queueFlags & VK_QUEUE_PROTECTED_BIT) ? "PROTECTED " : ""));
    }
}

static void debug_print_surface_info(VkPhysicalDevice device, VkSurfaceKHR surface,
                                     const char *indent = "")
{
    VkSurfaceCapabilitiesKHR surfaceCaps;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &surfaceCaps);
    LOG_DEBUG("%sSurface capabilities:\n", indent);
    LOG_DEBUG("%s  Minimum image count: %d\n", indent, surfaceCaps.minImageCount);
    LOG_DEBUG("%s  Current extent: %dx%d\n", indent, surfaceCaps.currentExtent.width,
              surfaceCaps.currentExtent.height);
    LOG_DEBUG("%s  Supported usage: %d\n", indent, surfaceCaps.supportedUsageFlags);

    uint32_t formatCount = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
    std::vector<VkSurfaceFormatKHR> surface_formats(formatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, &surface_formats[0]);
    LOG_DEBUG("%sSurface format count: %d\n", indent, formatCount);
    for (uint32_t i = 0; i < formatCount; ++i) {
        LOG_DEBUG("%s  Surface format index: %d\n", indent, i);
        LOG_DEBUG("%s    Format: %d\n", indent, surface_formats[i].format);
    }
}

static std::vector<uint8_t> read_shader_binary(const char *filename)
{
    FILE *stream = std::fopen(filename, "rb+");
    if (!stream) {
        LOG_ERROR("Could not open %s\n", filename);
        std::exit(EXIT_FAILURE);
    }

    std::fseek(stream, 0, SEEK_END);
    const uint32_t numBytes = std::ftell(stream) + 1;
    std::rewind(stream);

    std::vector<uint8_t> buffer(numBytes);
    buffer.resize(std::fread(&buffer[0], sizeof(uint8_t), numBytes, stream));
    std::fclose(stream);
    return buffer;
}

void load_shader_module(const VkDevice device, const char *filename, VkShaderModule *shader)
{
    assert(shader != nullptr);

    if (*shader != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyShaderModule(device, *shader, nullptr);
    }

    std::vector<uint8_t> byteCode = read_shader_binary(filename);
    assert(byteCode.size() % 4 == 0);

    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = byteCode.size();
    createInfo.pCode = (const uint32_t *)&byteCode[0];
    if (VK_SUCCESS != vkCreateShaderModule(device, &createInfo, nullptr, shader)) {
        LOG_ERROR("Failed to create shader module\n");
        std::exit(EXIT_FAILURE);
    }
}

void init_vulkan(VkuContext *vtx, bool showVulkanInfo)
{
    assert(vtx != nullptr);

    LOG_INFO("vku::init_vulkan: Initialising...\n");

    *vtx = {};  // Make sure everything is zero-initialized

    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, &extensions[0]);
    std::vector<const char *> extensionNames(extensionCount);
    for (uint32_t i = 0; i < extensionCount; ++i) {
        extensionNames[i] = extensions[i].extensionName;
    }
    if (showVulkanInfo) { debug_print_instance_info(""); }

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = nullptr;
    createInfo.enabledLayerCount = 0;
    createInfo.ppEnabledLayerNames = nullptr;
    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = &extensionNames[0];
    if (VK_SUCCESS != vkCreateInstance(&createInfo, nullptr, &vtx->instance)) {
        LOG_ERROR("Failed to create instance\n");
        std::exit(EXIT_FAILURE);
    }

    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(vtx->instance, &deviceCount, nullptr);
    std::vector<VkPhysicalDevice> devices(deviceCount);
    vkEnumeratePhysicalDevices(vtx->instance, &deviceCount, &devices[0]);
    if (showVulkanInfo) { LOG_DEBUG("Device count: %d\n", deviceCount); }

    uint32_t preferred = 0;
    std::vector<VkPhysicalDeviceProperties> device_props(deviceCount);
    for (uint32_t i = 0; i < deviceCount; ++i) {
        vkGetPhysicalDeviceProperties(devices[i], &device_props[i]);
        if (showVulkanInfo) { debug_print_device_info(devices[i], "  "); }
        if (device_props[i].deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) { preferred = i; }
    }
    vtx->physicalDevice = devices[preferred];

    VkPhysicalDeviceProperties deviceProps = {};
    vkGetPhysicalDeviceProperties(vtx->physicalDevice, &deviceProps);
    LOG_INFO("vku::init_vulkan: Vulkan version: %s (GPU: %s)\n",
             get_api_string(deviceProps.apiVersion).c_str(), deviceProps.deviceName);

    VkDeviceQueueCreateInfo queueInfo = {};
    queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueInfo.queueFamilyIndex = 0;
    queueInfo.queueCount = 1;
    queueInfo.pQueuePriorities = nullptr;

    uint32_t extensionCount2 = 0;
    vkEnumerateDeviceExtensionProperties(vtx->physicalDevice, nullptr, &extensionCount2, nullptr);
    std::vector<VkExtensionProperties> extensions2(extensionCount2);
    vkEnumerateDeviceExtensionProperties(vtx->physicalDevice, nullptr, &extensionCount2,
                                         &extensions2[0]);
    std::vector<const char *> extensionNames2(extensionCount2);
    for (uint32_t i = 0; i < extensionCount2; ++i) {
        extensionNames2[i] = extensions2[i].extensionName;
    }

    // Enable null descriptors via VK_EXT_robustness2 extension, to allow
    // VK_NULL_HANDLE to be used in for example vertex input buffer bindings.
    // TODO: Should display warning message if this extension is not supported
    VkPhysicalDeviceRobustness2FeaturesEXT robustness2Features = {};
    robustness2Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT;
    robustness2Features.nullDescriptor = true;

    VkDeviceCreateInfo createInfo2 = {};
    createInfo2.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo2.pNext = &robustness2Features;
    createInfo2.queueCreateInfoCount = 1;
    createInfo2.pQueueCreateInfos = &queueInfo;
    createInfo2.enabledLayerCount = 0;
    createInfo2.ppEnabledLayerNames = nullptr;
    createInfo2.enabledExtensionCount = extensionCount2;
    createInfo2.ppEnabledExtensionNames = &extensionNames2[0];
    createInfo2.pEnabledFeatures = nullptr;
    if (VK_SUCCESS != vkCreateDevice(vtx->physicalDevice, &createInfo2, nullptr, &vtx->device)) {
        LOG_ERROR("Failed to create device\n");
        std::exit(EXIT_FAILURE);
    }
    vkGetDeviceQueue(vtx->device, 0, 0, &vtx->queue);

    VkSemaphoreCreateInfo createInfo3 = {};
    createInfo3.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    for (uint32_t i = 0; i < 2; ++i) {
        if (VK_SUCCESS !=
            vkCreateSemaphore(vtx->device, &createInfo3, nullptr, &vtx->semaphores[i])) {
            LOG_ERROR("Failed to create semaphore\n");
            std::exit(EXIT_FAILURE);
        }
    }

    // Initialize Vulkan Memory Allocator (VMA)
    VmaAllocatorCreateInfo allocatorCreateInfo = {};
    allocatorCreateInfo.vulkanApiVersion = VK_API_VERSION_1_0;  // FIXME
    allocatorCreateInfo.physicalDevice = vtx->physicalDevice;
    allocatorCreateInfo.device = vtx->device;
    allocatorCreateInfo.instance = vtx->instance;
    vmaCreateAllocator(&allocatorCreateInfo, &vtx->allocator);

    LOG_INFO("vku::init_vulkan: Done\n");
}

void shutdown_vulkan(VkuContext *vtx)
{
    assert(vtx != nullptr);
    assert(vtx->device != VK_NULL_HANDLE);
    assert(vtx->instance != VK_NULL_HANDLE);

    LOG_INFO("vku::shutdown_vulkan: Shutting down...\n");

    if (vtx->surface != VK_NULL_HANDLE) {
        vkDestroySurfaceKHR(vtx->instance, vtx->surface, nullptr);
    }
    if (vtx->swapchain.swapchain != VK_NULL_HANDLE) {
        vkDestroySwapchainKHR(vtx->device, vtx->swapchain.swapchain, nullptr);
    }
    if (vtx->descriptorPool != VK_NULL_HANDLE) {
        vkDestroyDescriptorPool(vtx->device, vtx->descriptorPool, nullptr);
    }
    if (vtx->cmdPool != VK_NULL_HANDLE) {
        vkDestroyCommandPool(vtx->device, vtx->cmdPool, nullptr);
    }

    vmaDestroyAllocator(vtx->allocator);

    vkDestroySemaphore(vtx->device, vtx->semaphores[0], nullptr);
    vkDestroySemaphore(vtx->device, vtx->semaphores[1], nullptr);
    vkDestroyDevice(vtx->device, nullptr);
    vkDestroyInstance(vtx->instance, nullptr);

    LOG_INFO("vku::shutdown_vulkan: Done\n");
}

void create_swapchain(VkuContext *vtx, bool showSurfaceInfo)
{
    assert(vtx != nullptr);

    if (vtx->swapchain.swapchain != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroySwapchainKHR(vtx->device, vtx->swapchain.swapchain, nullptr);
        const uint32_t imageCount = uint32_t(vtx->swapchain.colorImages.size());
        for (uint32_t i = 0; i < imageCount; ++i) {
            vkDestroyImageView(vtx->device, vtx->swapchain.colorImageViews[i], nullptr);
            vkDestroyImage(vtx->device, vtx->swapchain.depthImages[i], nullptr);
            vkDestroyImageView(vtx->device, vtx->swapchain.depthImageViews[i], nullptr);
        }
        vtx->swapchain = {};
    }

    VkSurfaceCapabilitiesKHR surfaceCaps;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vtx->physicalDevice, vtx->surface, &surfaceCaps);
    if (showSurfaceInfo) { debug_print_surface_info(vtx->physicalDevice, vtx->surface); }
    vtx->swapchain.imageExtent = surfaceCaps.currentExtent;

    {
        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = vtx->surface;
        createInfo.minImageCount = 2;
        createInfo.imageFormat = VK_FORMAT_B8G8R8A8_UNORM;
        createInfo.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
        createInfo.imageExtent = vtx->swapchain.imageExtent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = surfaceCaps.supportedUsageFlags;
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = nullptr;
        createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = VK_NULL_HANDLE;
        if (VK_SUCCESS !=
            vkCreateSwapchainKHR(vtx->device, &createInfo, nullptr, &vtx->swapchain.swapchain)) {
            LOG_ERROR("Failed to create swapchain\n");
            std::exit(EXIT_FAILURE);
        }
    }

    uint32_t imageCount = 0;
    vkGetSwapchainImagesKHR(vtx->device, vtx->swapchain.swapchain, &imageCount, nullptr);

    vtx->swapchain.colorImages.resize(imageCount);
    vkGetSwapchainImagesKHR(vtx->device, vtx->swapchain.swapchain, &imageCount,
                            &vtx->swapchain.colorImages[0]);

    vtx->swapchain.colorImageViews.resize(imageCount);
    for (uint32_t i = 0; i < imageCount; ++i) {
        VkImageViewCreateInfo createInfo = {};
        const VkComponentMapping components = {};  // TODO
        const VkImageSubresourceRange subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = vtx->swapchain.colorImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = VK_FORMAT_B8G8R8A8_UNORM;
        createInfo.components = components;
        createInfo.subresourceRange = subresourceRange;
        if (VK_SUCCESS != vkCreateImageView(vtx->device, &createInfo, nullptr,
                                            &vtx->swapchain.colorImageViews[i])) {
            LOG_ERROR("Failed to create image view\n");
            std::exit(EXIT_FAILURE);
        }
    }

    vtx->swapchain.depthImages.resize(imageCount);
    for (uint32_t i = 0; i < imageCount; ++i) {
        VkImageCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        createInfo.imageType = VK_IMAGE_TYPE_2D;
        createInfo.format = VK_FORMAT_D32_SFLOAT_S8_UINT;
        createInfo.extent = {vtx->swapchain.imageExtent.width, vtx->swapchain.imageExtent.height,
                             1};
        createInfo.mipLevels = 1;
        createInfo.arrayLayers = 1;
        createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        createInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        createInfo.initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = VMA_MEMORY_USAGE_AUTO;
        VmaAllocation allocation;  // TODO: Need to store this somewhere
        if (VK_SUCCESS != vmaCreateImage(vtx->allocator, &createInfo, &allocInfo,
                                         &vtx->swapchain.depthImages[i], &allocation, nullptr)) {
            LOG_ERROR("Failed to create depth-stencil image\n");
            std::exit(EXIT_FAILURE);
        }
    }

    vtx->swapchain.depthImageViews.resize(imageCount);
    for (uint32_t i = 0; i < imageCount; ++i) {
        VkImageViewCreateInfo createInfo = {};
        const VkComponentMapping components = {};  // TODO
        const VkImageSubresourceRange subresourceRange = {
            VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, 0, 1, 0, 1};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = vtx->swapchain.depthImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = VK_FORMAT_D32_SFLOAT_S8_UINT;
        createInfo.components = components;
        createInfo.subresourceRange = subresourceRange;
        if (VK_SUCCESS != vkCreateImageView(vtx->device, &createInfo, nullptr,
                                            &vtx->swapchain.depthImageViews[i])) {
            LOG_ERROR("Failed to create depth image view\n");
            std::exit(EXIT_FAILURE);
        }
    }

    vkAcquireNextImageKHR(vtx->device, vtx->swapchain.swapchain, 0, vtx->semaphores[0], 0,
                          &vtx->swapIndex);
}

void create_descriptor_pool(VkuContext *vtx, uint32_t poolSize)
{
    assert(vtx != nullptr);

    if (vtx->descriptorPool != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyDescriptorPool(vtx->device, vtx->descriptorPool, nullptr);
    }

    const VkDescriptorPoolSize poolSizes[] = {{VK_DESCRIPTOR_TYPE_SAMPLER, poolSize},
                                              {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, poolSize},
                                              {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, poolSize},
                                              {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, poolSize},
                                              {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, poolSize},
                                              {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, poolSize},
                                              {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, poolSize},
                                              {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, poolSize},
                                              {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, poolSize},
                                              {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, poolSize},
                                              {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, poolSize}};
    const uint32_t poolSizeCount = sizeof(poolSizes) / sizeof(poolSizes[0]);

    VkDescriptorPoolCreateInfo descriptorPoolInfo = {};
    descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    descriptorPoolInfo.maxSets = poolSize * poolSizeCount;
    descriptorPoolInfo.poolSizeCount = poolSizeCount;
    descriptorPoolInfo.pPoolSizes = poolSizes;
    if (VK_SUCCESS !=
        vkCreateDescriptorPool(vtx->device, &descriptorPoolInfo, nullptr, &vtx->descriptorPool)) {
        LOG_ERROR("Failed to create descriptor pool\n");
        std::exit(EXIT_FAILURE);
    }
}

void create_command_pool(VkuContext *vtx)
{
    assert(vtx != nullptr);

    if (vtx->cmdPool != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyCommandPool(vtx->device, vtx->cmdPool, nullptr);
    }

    VkCommandPoolCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    if (VK_SUCCESS != vkCreateCommandPool(vtx->device, &createInfo, nullptr, &vtx->cmdPool)) {
        LOG_ERROR("Failed to create command pool\n");
        std::exit(EXIT_FAILURE);
    }
}

void create_command_buffers(VkuContext *vtx)
{
    assert(vtx != nullptr);
    assert(vtx->cmdPool != VK_NULL_HANDLE);

    if (vtx->cmdBuffers[0] != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkFreeCommandBuffers(vtx->device, vtx->cmdPool, 2, &vtx->cmdBuffers[0]);
    }

    VkCommandBufferAllocateInfo allocateInfo = {};
    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocateInfo.commandPool = vtx->cmdPool;
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandBufferCount = 2;
    if (VK_SUCCESS != vkAllocateCommandBuffers(vtx->device, &allocateInfo, &vtx->cmdBuffers[0])) {
        LOG_ERROR("Failed to allocate command buffers\n");
        std::exit(EXIT_FAILURE);
    }
}

void create_timestamp_query_pools(VkuContext *vtx, uint32_t poolSize)
{
    assert(vtx != nullptr);
    assert(poolSize > 0);

    if (vtx->timestampQueryPools[0] != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        for (uint32_t i = 0; i < 2; ++i) {
            vkDestroyQueryPool(vtx->device, vtx->timestampQueryPools[i], nullptr);
        }
    }

    for (uint32_t i = 0; i < 2; ++i) {
        VkQueryPoolCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
        createInfo.queryType = VK_QUERY_TYPE_TIMESTAMP;
        createInfo.queryCount = poolSize;
        if (VK_SUCCESS !=
            vkCreateQueryPool(vtx->device, &createInfo, nullptr, &vtx->timestampQueryPools[i])) {
            LOG_ERROR("Failed to create timestamp query pool\n");
            std::exit(EXIT_FAILURE);
        }
    }
}

void get_default_color_attachment_description(VkAttachmentDescription *attachmentDesc)
{
    assert(attachmentDesc != nullptr);

    *attachmentDesc = {};
    attachmentDesc->format = VK_FORMAT_B8G8R8A8_UNORM;
    attachmentDesc->samples = VK_SAMPLE_COUNT_1_BIT;
    attachmentDesc->loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    attachmentDesc->storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachmentDesc->stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachmentDesc->stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachmentDesc->initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachmentDesc->finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
}

void get_default_depth_stencil_attachment_description(VkAttachmentDescription *attachmentDesc)
{
    assert(attachmentDesc != nullptr);

    *attachmentDesc = {};
    attachmentDesc->format = VK_FORMAT_D32_SFLOAT_S8_UINT;
    attachmentDesc->samples = VK_SAMPLE_COUNT_1_BIT;
    attachmentDesc->loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    attachmentDesc->storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachmentDesc->stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachmentDesc->stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachmentDesc->initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    attachmentDesc->finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
}

void create_render_pass(const VkuContext &vtx,
                        const std::vector<VkAttachmentDescription> &attachments,
                        const std::vector<VkAttachmentReference> &colorAttachments,
                        const std::vector<VkAttachmentReference> &depthStencilAttachments,
                        VkRenderPass *renderPass)
{
    assert(attachments.size() > 0);
    assert(attachments.size() == colorAttachments.size() + depthStencilAttachments.size());
    assert(depthStencilAttachments.size() <= 1);
    assert(renderPass != nullptr);

    if (*renderPass != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyRenderPass(vtx.device, *renderPass, nullptr);
    }

    VkSubpassDescription subpassDesc = {};
    subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDesc.inputAttachmentCount = 0;
    subpassDesc.pInputAttachments = nullptr;
    subpassDesc.colorAttachmentCount = uint32_t(colorAttachments.size());
    subpassDesc.pColorAttachments = colorAttachments.size() ? &colorAttachments[0] : nullptr;
    subpassDesc.pResolveAttachments = nullptr;
    subpassDesc.pDepthStencilAttachment =
        depthStencilAttachments.size() ? &depthStencilAttachments[0] : nullptr;
    subpassDesc.preserveAttachmentCount = 0;
    subpassDesc.pPreserveAttachments = nullptr;

    VkRenderPassCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    createInfo.attachmentCount = uint32_t(attachments.size());
    createInfo.pAttachments = &attachments[0];
    createInfo.subpassCount = 1;
    createInfo.pSubpasses = &subpassDesc;
    createInfo.dependencyCount = 0;
    createInfo.pDependencies = nullptr;
    if (VK_SUCCESS != vkCreateRenderPass(vtx.device, &createInfo, nullptr, renderPass)) {
        LOG_ERROR("Failed to create render pass\n");
        std::exit(EXIT_FAILURE);
    }
}

void create_framebuffer(const VkuContext &vtx, const VkRenderPass renderPass,
                        const std::vector<VkImageView> &attachments, const VkExtent2D &extent,
                        VkFramebuffer *framebuffer)
{
    assert(renderPass != VK_NULL_HANDLE);
    assert(attachments.size() > 0);
    assert(framebuffer != nullptr);

    if (*framebuffer != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyFramebuffer(vtx.device, *framebuffer, nullptr);
    }

    VkFramebufferCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    createInfo.renderPass = renderPass;
    createInfo.attachmentCount = uint32_t(attachments.size());
    createInfo.pAttachments = &attachments[0];
    createInfo.width = extent.width;
    createInfo.height = extent.height;
    createInfo.layers = 1;
    if (VK_SUCCESS != vkCreateFramebuffer(vtx.device, &createInfo, nullptr, framebuffer)) {
        LOG_ERROR("Failed to create framebuffer\n");
        std::exit(EXIT_FAILURE);
    }
}

void add_pipeline_shader_stage(VkuGraphicsPipelineInfo *pipelineInfo,
                               const VkShaderStageFlagBits stage, const VkShaderModule shader)
{
    assert(pipelineInfo != nullptr);
    assert(shader != VK_NULL_HANDLE);

    VkPipelineShaderStageCreateInfo stageInfo = {};
    stageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    stageInfo.pName = "main";
    stageInfo.stage = stage;
    stageInfo.module = shader;
    pipelineInfo->stageInfos.push_back(stageInfo);
}

void get_default_pipeline_states(VkuGraphicsPipelineInfo *pipelineInfo)
{
    assert(pipelineInfo != nullptr);

    pipelineInfo->vertexInputInfo = {};
    pipelineInfo->vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    pipelineInfo->vertexInputInfo.vertexBindingDescriptionCount = 0;
    pipelineInfo->vertexInputInfo.pVertexBindingDescriptions = nullptr;
    pipelineInfo->vertexInputInfo.vertexAttributeDescriptionCount = 0;
    pipelineInfo->vertexInputInfo.pVertexAttributeDescriptions = nullptr;

    pipelineInfo->inputAssemblyInfo = {};
    pipelineInfo->inputAssemblyInfo.sType =
        VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    pipelineInfo->inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    pipelineInfo->inputAssemblyInfo.primitiveRestartEnable = 0;

    pipelineInfo->viewportInfo = {};
    pipelineInfo->viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    pipelineInfo->viewportInfo.viewportCount = 1;
    pipelineInfo->viewportInfo.pViewports = nullptr;  // Ignored when using dynamic viewport
    pipelineInfo->viewportInfo.scissorCount = 1;
    pipelineInfo->viewportInfo.pScissors = nullptr;  // Ignored when using dynamic scissor

    pipelineInfo->rasterizationInfo = {};
    pipelineInfo->rasterizationInfo.sType =
        VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    pipelineInfo->rasterizationInfo.depthClampEnable = VK_FALSE;
    pipelineInfo->rasterizationInfo.rasterizerDiscardEnable = VK_FALSE;
    pipelineInfo->rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
    pipelineInfo->rasterizationInfo.cullMode = VK_CULL_MODE_NONE;
    pipelineInfo->rasterizationInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    pipelineInfo->rasterizationInfo.depthBiasEnable = VK_FALSE;
    pipelineInfo->rasterizationInfo.depthBiasConstantFactor = 0;
    pipelineInfo->rasterizationInfo.depthBiasClamp = 0;
    pipelineInfo->rasterizationInfo.depthBiasSlopeFactor = 0;
    pipelineInfo->rasterizationInfo.lineWidth = 1.0f;

    pipelineInfo->multisampleInfo = {};
    pipelineInfo->multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    pipelineInfo->multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    pipelineInfo->multisampleInfo.sampleShadingEnable = VK_FALSE;
    pipelineInfo->multisampleInfo.minSampleShading = 1.0f;
    pipelineInfo->multisampleInfo.pSampleMask = nullptr;
    pipelineInfo->multisampleInfo.alphaToCoverageEnable = VK_FALSE;
    pipelineInfo->multisampleInfo.alphaToOneEnable = VK_FALSE;

    pipelineInfo->depthStencilInfo = {};
    const VkStencilOpState stencilOp = {
        VK_STENCIL_OP_KEEP, VK_STENCIL_OP_KEEP, VK_STENCIL_OP_KEEP, VK_COMPARE_OP_NEVER, 0, 0, 0};
    pipelineInfo->depthStencilInfo.sType =
        VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    pipelineInfo->depthStencilInfo.depthTestEnable = VK_FALSE;
    pipelineInfo->depthStencilInfo.depthWriteEnable = VK_TRUE;
    pipelineInfo->depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS;
    pipelineInfo->depthStencilInfo.depthBoundsTestEnable = VK_FALSE;
    pipelineInfo->depthStencilInfo.stencilTestEnable = VK_FALSE;
    pipelineInfo->depthStencilInfo.front = stencilOp;
    pipelineInfo->depthStencilInfo.back = stencilOp;
    pipelineInfo->depthStencilInfo.minDepthBounds = 0.0f;
    pipelineInfo->depthStencilInfo.maxDepthBounds = 1.0f;

    pipelineInfo->colorBlendInfo = {};
    pipelineInfo->colorBlendAttachmentState = {VK_FALSE,
                                               VK_BLEND_FACTOR_ONE,
                                               VK_BLEND_FACTOR_ZERO,
                                               VK_BLEND_OP_ADD,
                                               VK_BLEND_FACTOR_ONE,
                                               VK_BLEND_FACTOR_ZERO,
                                               VK_BLEND_OP_ADD,
                                               0xf};
    pipelineInfo->colorBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    pipelineInfo->colorBlendInfo.logicOpEnable = VK_FALSE;
    pipelineInfo->colorBlendInfo.logicOp = VK_LOGIC_OP_NO_OP;
    pipelineInfo->colorBlendInfo.attachmentCount = 1;
    pipelineInfo->colorBlendInfo.pAttachments = &pipelineInfo->colorBlendAttachmentState;
    pipelineInfo->colorBlendInfo.blendConstants[0] = 1.0f;
    pipelineInfo->colorBlendInfo.blendConstants[1] = 1.0f;
    pipelineInfo->colorBlendInfo.blendConstants[2] = 1.0f;
    pipelineInfo->colorBlendInfo.blendConstants[3] = 1.0f;

    pipelineInfo->dynamicInfo = {};
    pipelineInfo->dynamicStates.push_back(VK_DYNAMIC_STATE_VIEWPORT);
    pipelineInfo->dynamicStates.push_back(VK_DYNAMIC_STATE_SCISSOR);
    pipelineInfo->dynamicInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    pipelineInfo->dynamicInfo.dynamicStateCount = uint32_t(pipelineInfo->dynamicStates.size());
    pipelineInfo->dynamicInfo.pDynamicStates = &pipelineInfo->dynamicStates[0];
}

void add_vertex_input(VkuGraphicsPipelineInfo *pipelineInfo, uint32_t binding, uint32_t location,
                      const VkFormat format, uint32_t stride, uint32_t offset)
{
    assert(pipelineInfo != nullptr);
    assert(stride > 0);

    // Pre-allocate space to make sure pointers to elements remain valid
    pipelineInfo->inputBindings.reserve(8);
    assert(pipelineInfo->inputBindings.size() < 8);
    pipelineInfo->inputAttributes.reserve(8);
    assert(pipelineInfo->inputAttributes.size() < 8);

    VkVertexInputBindingDescription inputBinding = {};
    inputBinding.binding = binding;
    inputBinding.stride = stride;
    inputBinding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;  // TODO
    pipelineInfo->inputBindings.push_back(inputBinding);

    VkVertexInputAttributeDescription inputAttribute = {};
    inputAttribute.location = location;
    inputAttribute.binding = binding;
    inputAttribute.format = format;
    inputAttribute.offset = offset;
    pipelineInfo->inputAttributes.push_back(inputAttribute);

    pipelineInfo->vertexInputInfo.vertexBindingDescriptionCount =
        uint32_t(pipelineInfo->inputBindings.size());
    pipelineInfo->vertexInputInfo.pVertexBindingDescriptions = &pipelineInfo->inputBindings[0];
    pipelineInfo->vertexInputInfo.vertexAttributeDescriptionCount =
        uint32_t(pipelineInfo->inputAttributes.size());
    pipelineInfo->vertexInputInfo.pVertexAttributeDescriptions = &pipelineInfo->inputAttributes[0];
}

void create_graphics_pipeline(const VkuContext &vtx, const VkuGraphicsPipelineInfo &pipelineInfo,
                              const VkRenderPass renderPass, const VkDescriptorSetLayout setLayout,
                              uint32_t pushConstantRangeSize, VkuPipeline *pipeline)
{
    assert(pipeline != nullptr);
    assert(renderPass != VK_NULL_HANDLE);

    if (pipeline->pipeline != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyPipeline(vtx.device, pipeline->pipeline, nullptr);
        vkDestroyPipelineLayout(vtx.device, pipeline->pipelineLayout, nullptr);
    }

    {
        VkPipelineLayoutCreateInfo createInfo = {};
        const VkDescriptorSetLayout setLayouts[] = {setLayout};
        const VkPushConstantRange pushConstantRange = {VK_SHADER_STAGE_ALL, 0,
                                                       pushConstantRangeSize};
        createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        createInfo.setLayoutCount = setLayout ? 1 : 0;
        createInfo.pSetLayouts = setLayouts;
        createInfo.pushConstantRangeCount = pushConstantRangeSize ? 1 : 0;
        createInfo.pPushConstantRanges = &pushConstantRange;
        if (VK_SUCCESS !=
            vkCreatePipelineLayout(vtx.device, &createInfo, nullptr, &pipeline->pipelineLayout)) {
            LOG_ERROR("Failed to create pipeline layout\n");
            std::exit(EXIT_FAILURE);
        }
    }

    {
        VkGraphicsPipelineCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.stageCount = uint32_t(pipelineInfo.stageInfos.size());
        createInfo.pStages = &pipelineInfo.stageInfos[0];
        createInfo.pVertexInputState = &pipelineInfo.vertexInputInfo;
        createInfo.pInputAssemblyState = &pipelineInfo.inputAssemblyInfo;
        createInfo.pTessellationState = nullptr;
        createInfo.pViewportState = &pipelineInfo.viewportInfo;
        createInfo.pRasterizationState = &pipelineInfo.rasterizationInfo;
        createInfo.pMultisampleState = &pipelineInfo.multisampleInfo;
        createInfo.pDepthStencilState = &pipelineInfo.depthStencilInfo;
        createInfo.pColorBlendState = &pipelineInfo.colorBlendInfo;
        createInfo.pDynamicState = &pipelineInfo.dynamicInfo;
        createInfo.layout = pipeline->pipelineLayout;
        createInfo.renderPass = renderPass;
        createInfo.subpass = 0;
        createInfo.basePipelineHandle = VK_NULL_HANDLE;
        createInfo.basePipelineIndex = -1;
        if (VK_SUCCESS != vkCreateGraphicsPipelines(vtx.device, VK_NULL_HANDLE, 1, &createInfo,
                                                    nullptr, &pipeline->pipeline)) {
            LOG_ERROR("Failed to create graphics pipeline\n");
            std::exit(EXIT_FAILURE);
        }
    }
}

void create_compute_pipeline(const VkuContext &vtx, const VkShaderModule computeShader,
                             const VkDescriptorSetLayout setLayout, uint32_t pushConstantRangeSize,
                             VkuPipeline *pipeline)
{
    assert(pipeline != nullptr);
    assert(computeShader != VK_NULL_HANDLE);

    if (pipeline->pipeline != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyPipeline(vtx.device, pipeline->pipeline, nullptr);
        vkDestroyPipelineLayout(vtx.device, pipeline->pipelineLayout, nullptr);
    }

    {
        VkPipelineLayoutCreateInfo createInfo = {};
        const VkDescriptorSetLayout setLayouts[] = {setLayout};
        const VkPushConstantRange pushConstantRange = {VK_SHADER_STAGE_ALL, 0,
                                                       pushConstantRangeSize};
        createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        createInfo.setLayoutCount = setLayout ? 1 : 0;
        createInfo.pSetLayouts = setLayouts;
        createInfo.pushConstantRangeCount = pushConstantRangeSize ? 1 : 0;
        createInfo.pPushConstantRanges = &pushConstantRange;
        if (VK_SUCCESS !=
            vkCreatePipelineLayout(vtx.device, &createInfo, nullptr, &pipeline->pipelineLayout)) {
            LOG_ERROR("Failed to create pipeline layout\n");
            std::exit(EXIT_FAILURE);
        }
    }

    {
        VkComputePipelineCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
        createInfo.stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        createInfo.stage.pName = "main";
        createInfo.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
        createInfo.stage.module = computeShader;
        createInfo.layout = pipeline->pipelineLayout;
        createInfo.basePipelineHandle = VK_NULL_HANDLE;
        createInfo.basePipelineIndex = -1;
        if (VK_SUCCESS != vkCreateComputePipelines(vtx.device, VK_NULL_HANDLE, 1, &createInfo,
                                                   nullptr, &pipeline->pipeline)) {
            LOG_ERROR("Failed to create compute pipeline\n");
            std::exit(EXIT_FAILURE);
        }
    }
}

void create_buffer(const VkuContext &vtx, uint32_t numBytes, const VkBufferUsageFlags usage,
                   VkuBuffer *buffer)
{
    assert(usage != 0u);
    assert(buffer != nullptr);

    if (buffer->buffer != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vmaDestroyBuffer(vtx.allocator, buffer->buffer, buffer->allocation);
    }

    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = numBytes;
    bufferInfo.usage = usage;
    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_AUTO;
    allocInfo.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
    if (VK_SUCCESS != vmaCreateBuffer(vtx.allocator, &bufferInfo, &allocInfo, &buffer->buffer,
                                      &buffer->allocation, nullptr)) {
        LOG_ERROR("Failed to create or allocate buffer\n");
        std::exit(EXIT_FAILURE);
    }
}

void create_texture_2d(const VkuContext &vtx, const VkExtent2D &extent, const VkFormat format,
                       const VkImageUsageFlags usage, VkuTexture *texture, uint32_t aspectMask,
                       uint32_t mipLevels, VkSamplerAddressMode wrapMode)
{
    assert(usage != 0);
    assert(texture != nullptr);
    assert(aspectMask != 0);
    assert(mipLevels > 0);

    if (texture->image != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroySampler(vtx.device, texture->sampler, nullptr);
        vkDestroyImageView(vtx.device, texture->imageView, nullptr);
        for (uint32_t i = 0; i < texture->mipLevelViews.size(); ++i) {
            vkDestroyImageView(vtx.device, texture->mipLevelViews[i], nullptr);
        }
        texture->mipLevelViews.clear();
        vmaDestroyImage(vtx.allocator, texture->image, texture->allocation);
    }

    {
        VkImageCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        createInfo.imageType = VK_IMAGE_TYPE_2D;
        createInfo.format = format;
        createInfo.extent = {extent.width, extent.height, 1};
        createInfo.mipLevels = mipLevels;
        createInfo.arrayLayers = 1;
        createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        createInfo.usage = usage;
        createInfo.initialLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = VMA_MEMORY_USAGE_AUTO;
        if (VK_SUCCESS != vmaCreateImage(vtx.allocator, &createInfo, &allocInfo, &texture->image,
                                         &texture->allocation, nullptr)) {
            LOG_ERROR("Failed to create or allocate image for 2D texture\n");
            std::exit(EXIT_FAILURE);
        }
    }
    {
        VkImageViewCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = texture->image;
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = format;
        createInfo.subresourceRange.aspectMask = aspectMask;
        createInfo.subresourceRange.levelCount = mipLevels;
        createInfo.subresourceRange.layerCount = 1;
        if (VK_SUCCESS !=
            vkCreateImageView(vtx.device, &createInfo, nullptr, &texture->imageView)) {
            LOG_ERROR("Failed to create image view for 2D texture\n");
            std::exit(EXIT_FAILURE);
        }
    }
    if (mipLevels > 1) {
        // For sake of simplicity, we here create a new image view for each level,
        // even if texture->imageView could have been re-used for the first one
        texture->mipLevelViews.resize(mipLevels);
        for (uint32_t i = 0; i < mipLevels; ++i) {
            VkImageViewCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            createInfo.image = texture->image;
            createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
            createInfo.format = format;
            createInfo.subresourceRange.aspectMask = aspectMask;
            createInfo.subresourceRange.baseMipLevel = i;
            createInfo.subresourceRange.levelCount = mipLevels;
            createInfo.subresourceRange.layerCount = 1;
            if (VK_SUCCESS !=
                vkCreateImageView(vtx.device, &createInfo, nullptr, &texture->mipLevelViews[i])) {
                LOG_ERROR("Failed to create mip level image view for 2D texture\n");
                std::exit(EXIT_FAILURE);
            }
        }
    }
    {
        VkSamplerCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        if (aspectMask & VK_IMAGE_ASPECT_COLOR_BIT) {
            createInfo.magFilter = VK_FILTER_LINEAR;
            createInfo.minFilter = VK_FILTER_LINEAR;
            createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        } else {
            createInfo.magFilter = VK_FILTER_NEAREST;
            createInfo.minFilter = VK_FILTER_NEAREST;
            createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        }
        createInfo.addressModeU = wrapMode;
        createInfo.addressModeV = wrapMode;
        createInfo.addressModeW = wrapMode;
        createInfo.minLod = -1000;
        createInfo.maxLod = 1000;
        createInfo.maxAnisotropy = 1.0f;
        if (VK_SUCCESS != vkCreateSampler(vtx.device, &createInfo, nullptr, &texture->sampler)) {
            LOG_ERROR("Failed to create sampler for 2D texture\n");
            std::exit(EXIT_FAILURE);
        }
    }
}

void update_texture_2d(VkuContext *vtx, const VkCommandBuffer cmdBuffer, const VkuTexture &texture,
                       const VkExtent2D &extent, uint32_t numBytes, const void *imageData,
                       const uint32_t regionCount, const VkBufferImageCopy *regions)
{
    assert(vtx != nullptr);
    assert(numBytes > 0);
    assert(numBytes >= extent.width * extent.height);
    assert(imageData != nullptr);
    assert(!(regionCount && regions == nullptr));

    // Create staging buffer for upload
    VkuBuffer uploadBuffer = {};
    vku::create_buffer(*vtx, numBytes, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, &uploadBuffer);
    vtx->stagingBuffers[vtx->swapIndex].push_back(uploadBuffer);

    // Upload image data to staging buffer
    void *mappedData = nullptr;
    vmaMapMemory(vtx->allocator, uploadBuffer.allocation, &mappedData);
    std::memcpy(mappedData, imageData, numBytes);
    vmaUnmapMemory(vtx->allocator, uploadBuffer.allocation);

    if (cmdBuffer == VK_NULL_HANDLE) {
        vkResetCommandPool(vtx->device, vtx->cmdPool, 0);
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        vkBeginCommandBuffer(vtx->cmdBuffers[vtx->swapIndex], &beginInfo);
    }

    // Transit image layout for transfering
    VkImageMemoryBarrier imageBarrier;
    vku::get_image_barrier(texture.image, VK_IMAGE_LAYOUT_UNDEFINED,
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &imageBarrier);
    vkCmdPipelineBarrier(vtx->cmdBuffers[vtx->swapIndex], VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
                         VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0, 0, nullptr, 0, nullptr, 1,
                         &imageBarrier);

    // Copy image data from staging buffer to image of texture
    if (regionCount) {
        vkCmdCopyBufferToImage(vtx->cmdBuffers[vtx->swapIndex], uploadBuffer.buffer, texture.image,
                               VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, regionCount, regions);
    } else {
        VkBufferImageCopy region = {};
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.layerCount = 1;
        region.imageExtent.width = extent.width;
        region.imageExtent.height = extent.height;
        region.imageExtent.depth = 1;
        vkCmdCopyBufferToImage(vtx->cmdBuffers[vtx->swapIndex], uploadBuffer.buffer, texture.image,
                               VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
    }

    // Transit image layout for usage in shaders
    vku::get_image_barrier(texture.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                           VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, &imageBarrier);
    vkCmdPipelineBarrier(vtx->cmdBuffers[vtx->swapIndex], VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
                         VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0, 0, nullptr, 0, nullptr, 1,
                         &imageBarrier);

    if (cmdBuffer == VK_NULL_HANDLE) {
        VkSubmitInfo endInfo = {};
        endInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        endInfo.commandBufferCount = 1;
        endInfo.pCommandBuffers = &vtx->cmdBuffers[vtx->swapIndex];
        vkEndCommandBuffer(vtx->cmdBuffers[vtx->swapIndex]);
        vkQueueSubmit(vtx->queue, 1, &endInfo, VK_NULL_HANDLE);
    }
}

template <typename ScalarType>
static void generate_mipmap_rgba(const ScalarType *imageData, uint32_t width, uint32_t height,
                                 std::vector<ScalarType> &mipmapData,
                                 std::vector<VkBufferImageCopy> &regions)
{
    assert(imageData != nullptr);
    assert(width > 0 && height > 0);

    mipmapData.clear();
    regions.clear();

    const uint32_t numBytes = uint32_t(width * height * 4 * sizeof(ScalarType));
    mipmapData.resize(width * height * 4);
    std::memcpy(&mipmapData[0], imageData, numBytes);

    VkBufferImageCopy region = {};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.layerCount = 1;
    region.imageExtent.width = width;
    region.imageExtent.height = height;
    region.imageExtent.depth = 1;
    regions.push_back(region);

    uint32_t prevOffset = 0, mipLevel = 0;
    while (width > 1 && height > 1) {
        const uint32_t offset = uint32_t(mipmapData.size());

        for (uint32_t y = 0; y < height; y += 2) {
            for (uint32_t x = 0; x < width; x += 2) {
                for (uint32_t c = 0; c < 4; ++c) {
                    const float texel0 =
                        mipmapData[prevOffset + ((y + 0) * width + (x + 0)) * 4 + c];
                    const float texel1 =
                        mipmapData[prevOffset + ((y + 0) * width + (x + 1)) * 4 + c];
                    const float texel2 =
                        mipmapData[prevOffset + ((y + 1) * width + (x + 0)) * 4 + c];
                    const float texel3 =
                        mipmapData[prevOffset + ((y + 1) * width + (x + 1)) * 4 + c];
                    const float value = (texel0 + texel1 + texel2 + texel3) * 0.25f;
                    mipmapData.push_back(ScalarType(value));
                }
            }
        }
        width /= 2, height /= 2;
        prevOffset = offset;
        mipLevel += 1;

        VkBufferImageCopy region = {};
        region.bufferOffset = offset * sizeof(ScalarType);
        region.bufferRowLength = 0;
        region.bufferImageHeight = 0;
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = mipLevel;
        region.imageSubresource.layerCount = 1;
        region.imageExtent.width = width;
        region.imageExtent.height = height;
        region.imageExtent.depth = 1;
        regions.push_back(region);
    }
}

void generate_mipmap_rgba8u(const uint8_t *imageData, uint32_t width, uint32_t height,
                            std::vector<uint8_t> &mipmapData,
                            std::vector<VkBufferImageCopy> &regions)
{
    generate_mipmap_rgba(imageData, width, height, mipmapData, regions);
}

void generate_mipmap_rgba32f(const float *imageData, uint32_t width, uint32_t height,
                             std::vector<float> &mipmapData,
                             std::vector<VkBufferImageCopy> &regions)
{
    generate_mipmap_rgba(imageData, width, height, mipmapData, regions);
}

void add_layout_binding(VkuLayoutInfo *layoutInfo, uint32_t binding,
                        const VkDescriptorType descriptorType, uint32_t descriptorCount,
                        const VkShaderStageFlagBits stageFlags)
{
    assert(layoutInfo != nullptr);

    VkDescriptorSetLayoutBinding layoutBinding = {};
    layoutBinding.binding = binding;
    layoutBinding.descriptorType = descriptorType;
    layoutBinding.descriptorCount = descriptorCount;
    layoutBinding.stageFlags = stageFlags;
    layoutInfo->bindings.push_back(layoutBinding);
}

void create_descriptor_set_layout(const VkuContext &vtx, const VkuLayoutInfo &layoutInfo,
                                  VkDescriptorSetLayout *setLayout)
{
    assert(layoutInfo.bindings.size() > 0);
    assert(setLayout != nullptr);

    if (*setLayout != VK_NULL_HANDLE) {
        // Clean up old objects before creating new ones
        vkDestroyDescriptorSetLayout(vtx.device, *setLayout, nullptr);
    }

    VkDescriptorSetLayoutCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR;
    createInfo.bindingCount = uint32_t(layoutInfo.bindings.size());
    createInfo.pBindings = &layoutInfo.bindings[0];
    if (VK_SUCCESS != vkCreateDescriptorSetLayout(vtx.device, &createInfo, nullptr, setLayout)) {
        LOG_ERROR("Failed to create descriptor set layout\n");
        std::exit(EXIT_FAILURE);
    }
}

void add_descriptor_binding(VkuPushDescriptorInfo *pdInfo, uint32_t binding,
                            const VkDescriptorType descriptorType, const VkuBuffer &buffer,
                            const VkDeviceSize bufferOffset, const VkDeviceSize bufferRange)
{
    assert(pdInfo != nullptr);
    //assert(buffer.buffer != VK_NULL_HANDLE);  // Allowed with nullDescriptor enabled

    // Pre-allocate space to make sure pointers to elements remain valid
    pdInfo->bufferInfos.reserve(32);
    assert(pdInfo->bufferInfos.size() < 32);

    VkDescriptorBufferInfo bufferInfo = {};
    bufferInfo.buffer = buffer.buffer;
    bufferInfo.offset = bufferOffset;
    bufferInfo.range = bufferRange;
    pdInfo->bufferInfos.push_back(bufferInfo);

    VkWriteDescriptorSet descriptorWrite = {};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = 0;  // Ignored for push descriptors
    descriptorWrite.dstBinding = binding;
    descriptorWrite.descriptorCount = 1;
    descriptorWrite.descriptorType = descriptorType;
    descriptorWrite.pBufferInfo = &pdInfo->bufferInfos.back();
    pdInfo->descriptorWrites.push_back(descriptorWrite);
}

void add_descriptor_binding(VkuPushDescriptorInfo *pdInfo, uint32_t binding,
                            const VkDescriptorType descriptorType,
                            const VkDescriptorImageInfo &imageInfo)
{
    assert(pdInfo != nullptr);
    //assert(imageInfo.imageView != VK_NULL_HANDLE);  // Allowed with nullDescriptor enabled

    // Pre-allocate space to make sure pointers to elements remain valid
    pdInfo->imageInfos.reserve(32);
    assert(pdInfo->imageInfos.size() < 32);

    pdInfo->imageInfos.push_back(imageInfo);

    VkWriteDescriptorSet descriptorWrite = {};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = 0;  // Ignored for push descriptors
    descriptorWrite.dstBinding = binding;
    descriptorWrite.descriptorCount = 1;
    descriptorWrite.descriptorType = descriptorType;
    descriptorWrite.pImageInfo = &pdInfo->imageInfos.back();
    pdInfo->descriptorWrites.push_back(descriptorWrite);
}

void add_descriptor_binding(VkuPushDescriptorInfo *pdInfo, uint32_t binding,
                            const VkDescriptorType descriptorType, const VkuTexture &texture,
                            const VkImageLayout imageLayout, uint32_t mipLevel)
{
    assert(mipLevel == 0 || mipLevel < texture.mipLevelViews.size());

    if (mipLevel > 0) {
        add_descriptor_binding(pdInfo, binding, descriptorType,
                               {texture.sampler, texture.mipLevelViews[mipLevel], imageLayout});
    } else {
        add_descriptor_binding(pdInfo, binding, descriptorType,
                               {texture.sampler, texture.imageView, imageLayout});
    }
}

void get_image_barrier(const VkImage image, const VkImageLayout oldLayout,
                       const VkImageLayout newLayout, VkImageMemoryBarrier *imageBarrier)
{
    assert(image != VK_NULL_HANDLE);
    assert(imageBarrier != nullptr);

    *imageBarrier = {};
    imageBarrier->sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageBarrier->srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
    imageBarrier->dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
    imageBarrier->oldLayout = oldLayout;
    imageBarrier->newLayout = newLayout;
    imageBarrier->image = image;
    imageBarrier->subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageBarrier->subresourceRange.levelCount = 1;
    imageBarrier->subresourceRange.layerCount = 1;
}

void swap_buffers(VkuContext *vtx)
{
    assert(vtx != nullptr);

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &vtx->semaphores[1];
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &vtx->swapchain.swapchain;
    presentInfo.pImageIndices = &vtx->swapIndex;
    presentInfo.pResults = nullptr;
    if (VK_SUCCESS != vkQueuePresentKHR(vtx->queue, &presentInfo)) {
        LOG_ERROR("Failed to swap buffers\n");
        return;  // TODO: Should re-create swapchain, etc.
    }

    vkQueueWaitIdle(vtx->queue);

    const uint64_t timeout_ns = uint64_t(1e8);
    vkAcquireNextImageKHR(vtx->device, vtx->swapchain.swapchain, timeout_ns, vtx->semaphores[0], 0,
                          &vtx->swapIndex);

    if (vtx->stagingBuffers[vtx->swapIndex].size() > 0) {
        //vkDeviceWaitIdle(vtx.device);  // Flush pending transfers
        // Remove staging buffers with completed transfers, to prepare for next frame
        for (const auto &buffer : vtx->stagingBuffers[vtx->swapIndex]) {
            vmaDestroyBuffer(vtx->allocator, buffer.buffer, buffer.allocation);
        }
        vtx->stagingBuffers[vtx->swapIndex].clear();
    }
}

}  // namespace vku
