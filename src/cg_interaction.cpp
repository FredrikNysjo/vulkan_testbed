/// @file
/// @brief Data types and utility functions for handling interaction
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms of the MIT
/// license. See the LICENSE.md file for details.
///

#include "cg_interaction.h"

namespace cg {

void trackball_move(Trackball &trackball, float x, float y, const glm::mat4 &worldFromView)
{
    if (!trackball.tracking) return;

    const glm::vec2 motion = glm::vec2(x, y) - trackball.center;
    if (glm::abs(motion.x) < 1.0f && glm::abs(motion.y) < 1.0f) return;
    const glm::vec2 theta = trackball.speed * glm::clamp(motion, -trackball.clamp, trackball.clamp);
    const glm::quat deltaX =
        glm::angleAxis(theta.x, glm::mat3(worldFromView) * glm::vec3(0.0f, 1.0f, 0.0f));
    const glm::quat deltaY =
        glm::angleAxis(theta.y, glm::mat3(worldFromView) * glm::vec3(1.0f, 0.0f, 0.0f));

    glm::quat q = deltaY * deltaX * trackball.orient;
    q = (glm::length(q) > 0.0f) ? glm::normalize(q) : glm::quat(1.0f, 0.0f, 0.0f, 0.0f);

    // Note: final quaternion should be in positive hemisphere
    trackball.orient = (q.w >= 0.0f) ? q : -q;
    trackball.center = glm::vec2(x, y);
}

void panning_move_xy(Panning &panning, float x, float y, const glm::mat4 &worldFromView)
{
    if (!panning.panning) return;

    const float motionX = (x - panning.center.x) * panning.speedXY;
    const float motionY = (panning.center.y - y) * panning.speedXY;

    panning.translation += glm::mat3(worldFromView) * glm::vec3(motionX, motionY, 0.0f);
    panning.center = glm::vec2(x, y);
}

void panning_move_z(Panning &panning, float zOffset, const glm::mat4 &worldFromView)
{
    const float motionZ = zOffset * panning.speedZ;

    panning.translation += glm::mat3(worldFromView) * glm::vec3(0.0f, 0.0f, motionZ);
}

}  // namespace cg
