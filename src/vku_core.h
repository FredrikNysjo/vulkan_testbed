/// @file
/// @brief  Abstractions for the Vulkan API
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <vk_mem_alloc.h>

#include <vector>

#define VKU_MAKE_NON_COPYABLE(STRUCT_NAME)                                                         \
    STRUCT_NAME(const STRUCT_NAME &) = delete;                                                     \
    STRUCT_NAME &operator=(const STRUCT_NAME &) = delete;

namespace vku {

struct VkuSwapchain {
    VkSwapchainKHR swapchain;
    std::vector<VkImage> colorImages;
    std::vector<VkImageView> colorImageViews;
    std::vector<VkImage> depthImages;
    std::vector<VkImageView> depthImageViews;
    VkExtent2D imageExtent;
};

struct VkuGraphicsPipelineInfo {
    std::vector<VkPipelineShaderStageCreateInfo> stageInfos;
    std::vector<VkVertexInputBindingDescription> inputBindings;
    std::vector<VkVertexInputAttributeDescription> inputAttributes;
    VkPipelineVertexInputStateCreateInfo vertexInputInfo;
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
    VkPipelineViewportStateCreateInfo viewportInfo;
    VkPipelineRasterizationStateCreateInfo rasterizationInfo;
    VkPipelineMultisampleStateCreateInfo multisampleInfo;
    VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
    VkPipelineColorBlendStateCreateInfo colorBlendInfo;
    VkPipelineColorBlendAttachmentState colorBlendAttachmentState;
    VkPipelineDynamicStateCreateInfo dynamicInfo;
    std::vector<VkDynamicState> dynamicStates;
    VKU_MAKE_NON_COPYABLE(VkuGraphicsPipelineInfo);
};

struct VkuPipeline {
    VkPipeline pipeline;
    VkPipelineLayout pipelineLayout;
};

struct VkuBuffer {
    VkBuffer buffer;
    VmaAllocation allocation;
};

struct VkuTexture {
    VkImage image;
    VkImageView imageView;
    std::vector<VkImageView> mipLevelViews;
    VkSampler sampler;
    VmaAllocation allocation;
};

struct VkuLayoutInfo {
    std::vector<VkDescriptorSetLayoutBinding> bindings;
};

struct VkuPushDescriptorInfo {
    std::vector<VkDescriptorBufferInfo> bufferInfos;
    std::vector<VkDescriptorImageInfo> imageInfos;
    std::vector<VkWriteDescriptorSet> descriptorWrites;
    VKU_MAKE_NON_COPYABLE(VkuPushDescriptorInfo);
};

struct VkuDrawInfo {
    std::vector<VkBuffer> vertexBuffers;
    std::vector<VkDeviceSize> vertexOffsets;
    VkBuffer indexBuffer;
    VkDeviceSize indexOffset;
    VkIndexType indexType;
    uint32_t count;
};

struct VkuContext {
    VkInstance instance;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    VkQueue queue;
    VkSemaphore semaphores[2];
    VkSurfaceKHR surface;
    VkuSwapchain swapchain;
    uint32_t swapIndex;
    VkCommandPool cmdPool;
    VkCommandBuffer cmdBuffers[2];
    VkDescriptorPool descriptorPool;
    VkQueryPool timestampQueryPools[2];
    std::vector<VkuBuffer> stagingBuffers[2];
    VmaAllocator allocator;
    PFN_vkCmdPushDescriptorSetKHR vkCmdPushDescriptorSetKHR;
};

void load_shader_module(const VkDevice device, const char *filename, VkShaderModule *shader);

void init_vulkan(VkuContext *vtx, bool showVulkanInfo = false);

void shutdown_vulkan(VkuContext *vtx);

void create_swapchain(VkuContext *vtx, bool showSurfaceInfo = false);

void create_descriptor_pool(VkuContext *vtx, uint32_t poolSize = 1024);

void create_command_pool(VkuContext *vtx);

void create_command_buffers(VkuContext *vtx);

void create_timestamp_query_pools(VkuContext *vtx, uint32_t poolSize = 128);

void get_default_color_attachment_description(VkAttachmentDescription *attachmentDesc);

void get_default_depth_stencil_attachment_description(VkAttachmentDescription *attachmentDesc);

void create_render_pass(const VkuContext &vtx,
                        const std::vector<VkAttachmentDescription> &attachments,
                        const std::vector<VkAttachmentReference> &colorAttachments,
                        const std::vector<VkAttachmentReference> &depthStencilAttachments,
                        VkRenderPass *renderPass);

void create_framebuffer(const VkuContext &vtx, const VkRenderPass renderPass,
                        const std::vector<VkImageView> &attachments, const VkExtent2D &extent,
                        VkFramebuffer *framebuffer);

void add_pipeline_shader_stage(VkuGraphicsPipelineInfo *pipelineInfo,
                               const VkShaderStageFlagBits stage, const VkShaderModule shader);

void get_default_pipeline_states(VkuGraphicsPipelineInfo *pipelineInfo);

void add_vertex_input(VkuGraphicsPipelineInfo *pipelineInfo, uint32_t binding, uint32_t location,
                      const VkFormat format, uint32_t stride, uint32_t offset = 0);

void create_graphics_pipeline(const VkuContext &vtx, const VkuGraphicsPipelineInfo &pipelineInfo,
                              const VkRenderPass renderPass, const VkDescriptorSetLayout setLayout,
                              uint32_t pushConstantRangeSize, VkuPipeline *pipeline);

void create_compute_pipeline(const VkuContext &vtx, const VkShaderModule computeShader,
                             const VkDescriptorSetLayout setLayout, uint32_t pushConstantRangeSize,
                             VkuPipeline *pipeline);

void create_buffer(const VkuContext &vtx, uint32_t numBytes, const VkBufferUsageFlags usage,
                   VkuBuffer *buffer);

void create_texture_2d(const VkuContext &vtx, const VkExtent2D &extent, const VkFormat format,
                       const VkImageUsageFlags usage, VkuTexture *texture,
                       uint32_t aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, uint32_t mipLevels = 1,
                       VkSamplerAddressMode wrapMode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);

void update_texture_2d(VkuContext *vtx, const VkCommandBuffer cmdBuffer, const VkuTexture &texture,
                       const VkExtent2D &extent, uint32_t numBytes, const void *imageData,
                       const uint32_t regionCount=0, const VkBufferImageCopy *regions = nullptr);

void generate_mipmap_rgba8u(const uint8_t *imageData, uint32_t width, uint32_t height,
                            std::vector<uint8_t> &mipmapData,
                            std::vector<VkBufferImageCopy> &regions);

void generate_mipmap_rgba32f(const float *imageData, uint32_t width, uint32_t height,
                             std::vector<float> &mipmapData,
                             std::vector<VkBufferImageCopy> &regions);

void add_layout_binding(VkuLayoutInfo *layoutInfo, uint32_t binding,
                        const VkDescriptorType descriptorType, uint32_t descriptorCount = 1,
                        const VkShaderStageFlagBits stageFlags = VK_SHADER_STAGE_ALL);

void create_descriptor_set_layout(const VkuContext &vtx, const VkuLayoutInfo &layoutInfo,
                                  VkDescriptorSetLayout *descriptorSetLayout);

void add_descriptor_binding(VkuPushDescriptorInfo *pdInfo, uint32_t binding,
                            const VkDescriptorType descriptorType, const VkuBuffer &buffer,
                            const VkDeviceSize bufferOffset = 0,
                            const VkDeviceSize bufferRange = VK_WHOLE_SIZE);

void add_descriptor_binding(VkuPushDescriptorInfo *pdInfo, uint32_t binding,
                            const VkDescriptorType descriptorType,
                            const VkDescriptorImageInfo &imageInfo);

void add_descriptor_binding(VkuPushDescriptorInfo *pdInfo, uint32_t binding,
                            const VkDescriptorType descriptorType, const VkuTexture &texture,
                            const VkImageLayout imageLayout, uint32_t mipLevel = 0);

void get_image_barrier(const VkImage image, const VkImageLayout oldLayout,
                       const VkImageLayout newLayout, VkImageMemoryBarrier *imageBarrier);

void swap_buffers(VkuContext *vtx);

}  // namespace vku
