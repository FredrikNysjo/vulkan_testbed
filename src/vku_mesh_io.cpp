/// @file
/// @brief Data structures and functions for mesh I/O
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vku_mesh_io.h"

#include <cstdio>
#include <cstdlib>
#undef NDEBUG
#include <cassert>

#define LOG_INFO(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_DEBUG(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_ERROR(...) std::fprintf(stderr, __VA_ARGS__)

namespace vku {

void read_obj(const char *filename, OBJMesh *mesh)
{
    assert(mesh != nullptr);

    FILE *stream = std::fopen(filename, "rb");
    if (!stream) {
        LOG_ERROR("Could not open %s\n", filename);
        std::exit(EXIT_FAILURE);
    }

    char line[256] = {0};
    while (std::fgets(line, 256, stream)) {
        float x, y, z;
        if ((line[0] == 'v') && (std::sscanf(line, "v %f %f %f", &x, &y, &z) == 3)) {
            mesh->positions.push_back(x);
            mesh->positions.push_back(y);
            mesh->positions.push_back(z);
            mesh->positions.push_back(1.0f);  // Pad to vec4
            continue;
        }
        int32_t i0, i1, i2;
        if ((line[0] == 'f') && (std::sscanf(line, "f %d %d %d", &i0, &i1, &i2) == 3)) {
            mesh->indices.push_back(i0 - 1);
            mesh->indices.push_back(i1 - 1);
            mesh->indices.push_back(i2 - 1);
        }
        // Ignore other lines
    }
    std::fclose(stream);
}

}  // namespace vku
