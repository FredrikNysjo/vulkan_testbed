/// @file
/// @brief Helper functions for dealing with ImGui in Vulkan
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "vku_core.h"

namespace vku {

void init_imgui(const VkuContext &vtx, const VkRenderPass renderPass, GLFWwindow *window,
                bool installCallbacks = false);

void shutdown_imgui(const VkuContext &vtx);

}  // namespace vku