/// @file
/// @brief  Main application
///
/// @section LICENSE
///
/// Copyright (c) 2022 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vku_core.h"
#include "vku_imgui.h"
#include "vku_mesh_io.h"
#include "cg_interaction.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_vulkan.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <cstdlib>
#include <cstring>
#include <string>
#include <unordered_map>
#undef NDEBUG
#include <cassert>

#define LOG_INFO(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_DEBUG(...) std::fprintf(stdout, __VA_ARGS__)
#define LOG_ERROR(...) std::fprintf(stderr, __VA_ARGS__)

#define SHOW_MESH_SCENE 1
#define SHOW_TRIANGLE_SCENE 0
#define SHOW_BILLBOARD_SCENE 0

struct SceneUniforms {
    glm::mat4 projFromView = glm::mat4(1.0f);
    glm::mat4 viewFromWorld = glm::mat4(1.0f);
    glm::mat4 worldFromLocal = glm::mat4(1.0f);
};

struct Window {
    uint32_t width = 512;
    uint32_t height = 512;
    GLFWwindow *id = nullptr;
    float elapsedTime = 0.0f;
};

struct Context {
    Window window;
    vku::VkuContext vtx;
    std::unordered_map<std::string, VkShaderModule> shaders;
    std::unordered_map<std::string, vku::VkuPipeline> pipelines;
    std::unordered_map<std::string, VkDescriptorSetLayout> setLayouts;
    std::unordered_map<std::string, VkRenderPass> renderPasses;
    std::unordered_map<std::string, VkFramebuffer> framebuffers;
    std::unordered_map<std::string, vku::VkuBuffer> buffers;
    std::unordered_map<std::string, vku::VkuTexture> textures;
    std::unordered_map<std::string, vku::VkuDrawInfo> drawInfos;
    vku::OBJMesh mesh;
    SceneUniforms sceneUniforms;
    cg::Trackball trackball;
    cg::Panning panning;
};

std::string shader_dir(void)
{
    const char *value = std::getenv("VKTEST_ROOT");
    std::string rootDir = value ? value : "";
    if (rootDir.empty()) { rootDir = ".."; }
    return rootDir + "/src/shaders/";
}

void setup_vulkan_required_objects(Context &ctx)
{
    assert(ctx.vtx.swapchain.swapchain == VK_NULL_HANDLE);
    assert(ctx.vtx.descriptorPool == VK_NULL_HANDLE);
    assert(ctx.vtx.cmdPool == VK_NULL_HANDLE);

    vku::create_swapchain(&ctx.vtx);
    vku::create_descriptor_pool(&ctx.vtx);
    vku::create_command_pool(&ctx.vtx);
}

void setup_vulkan_command_buffers(Context &ctx)
{
    vku::create_command_buffers(&ctx.vtx);
}

void setup_vulkan_render_passes(Context &ctx)
{
    {
        std::vector<VkAttachmentDescription> attachments(2);
        vku::get_default_color_attachment_description(&attachments[0]);
        vku::get_default_depth_stencil_attachment_description(&attachments[1]);
        const std::vector<VkAttachmentReference> colorAttachments = {
            {0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL}};
        const std::vector<VkAttachmentReference> depthStencilAttachments = {
            {1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL}};
        vku::create_render_pass(ctx.vtx, attachments, colorAttachments, depthStencilAttachments,
                                &ctx.renderPasses["main"]);
    }
}

void setup_vulkan_framebuffers(Context &ctx)
{
    {
        const std::vector<VkImageView> attachments0 = {ctx.vtx.swapchain.colorImageViews[0],
                                                       ctx.vtx.swapchain.depthImageViews[0]};
        const std::vector<VkImageView> attachments1 = {ctx.vtx.swapchain.colorImageViews[1],
                                                       ctx.vtx.swapchain.depthImageViews[1]};
        vku::create_framebuffer(ctx.vtx, ctx.renderPasses["main"], attachments0,
                                ctx.vtx.swapchain.imageExtent, &ctx.framebuffers["main_0"]);
        vku::create_framebuffer(ctx.vtx, ctx.renderPasses["main"], attachments1,
                                ctx.vtx.swapchain.imageExtent, &ctx.framebuffers["main_1"]);
    }
}

void setup_vulkan_shaders(Context &ctx)
{
    vku::load_shader_module(ctx.vtx.device, (shader_dir() + "draw_triangle.vert.spv").c_str(),
                            &ctx.shaders["draw_triangle.vert"]);
    vku::load_shader_module(ctx.vtx.device, (shader_dir() + "draw_triangle.frag.spv").c_str(),
                            &ctx.shaders["draw_triangle.frag"]);

    vku::load_shader_module(ctx.vtx.device, (shader_dir() + "draw_mesh.vert.spv").c_str(),
                            &ctx.shaders["draw_mesh.vert"]);
    vku::load_shader_module(ctx.vtx.device, (shader_dir() + "draw_mesh.frag.spv").c_str(),
                            &ctx.shaders["draw_mesh.frag"]);

    vku::load_shader_module(ctx.vtx.device, (shader_dir() + "draw_billboard.vert.spv").c_str(),
                            &ctx.shaders["draw_billboard.vert"]);
    vku::load_shader_module(ctx.vtx.device, (shader_dir() + "draw_billboard.frag.spv").c_str(),
                            &ctx.shaders["draw_billboard.frag"]);
}

void setup_vulkan_pipelines(Context &ctx)
{
    {
        vku::VkuGraphicsPipelineInfo pipelineInfo = {};
        vku::add_pipeline_shader_stage(&pipelineInfo, VK_SHADER_STAGE_VERTEX_BIT,
                                       ctx.shaders["draw_triangle.vert"]);
        vku::add_pipeline_shader_stage(&pipelineInfo, VK_SHADER_STAGE_FRAGMENT_BIT,
                                       ctx.shaders["draw_triangle.frag"]);
        vku::get_default_pipeline_states(&pipelineInfo);
        vku::create_graphics_pipeline(ctx.vtx, pipelineInfo, ctx.renderPasses["main"],
                                      VK_NULL_HANDLE, 0, &ctx.pipelines["draw_triangle"]);
    }
    {
        vku::VkuLayoutInfo layoutInfo = {};
        vku::add_layout_binding(&layoutInfo, 0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER);
        vku::add_layout_binding(&layoutInfo, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
        vku::create_descriptor_set_layout(ctx.vtx, layoutInfo, &ctx.setLayouts["draw_mesh"]);

        vku::VkuGraphicsPipelineInfo pipelineInfo = {};
        vku::add_pipeline_shader_stage(&pipelineInfo, VK_SHADER_STAGE_VERTEX_BIT,
                                       ctx.shaders["draw_mesh.vert"]);
        vku::add_pipeline_shader_stage(&pipelineInfo, VK_SHADER_STAGE_FRAGMENT_BIT,
                                       ctx.shaders["draw_mesh.frag"]);
        vku::get_default_pipeline_states(&pipelineInfo);
        pipelineInfo.depthStencilInfo.depthTestEnable = true;

        const std::vector<VkVertexInputBindingDescription> vertexBindings = {
            {0, sizeof(glm::vec4), VK_VERTEX_INPUT_RATE_VERTEX}};
        const std::vector<VkVertexInputAttributeDescription> vertexAttributes = {
            {0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0}};
        pipelineInfo.vertexInputInfo.vertexBindingDescriptionCount = 1;
        pipelineInfo.vertexInputInfo.pVertexBindingDescriptions = &vertexBindings[0];
        pipelineInfo.vertexInputInfo.vertexAttributeDescriptionCount = 1;
        pipelineInfo.vertexInputInfo.pVertexAttributeDescriptions = &vertexAttributes[0];

        vku::create_graphics_pipeline(ctx.vtx, pipelineInfo, ctx.renderPasses["main"],
                                      ctx.setLayouts["draw_mesh"], 0, &ctx.pipelines["draw_mesh"]);
    }
    {
        vku::VkuLayoutInfo layoutInfo = {};
        vku::add_layout_binding(&layoutInfo, 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
        vku::create_descriptor_set_layout(ctx.vtx, layoutInfo, &ctx.setLayouts["draw_billboard"]);

        vku::VkuGraphicsPipelineInfo pipelineInfo = {};
        vku::add_pipeline_shader_stage(&pipelineInfo, VK_SHADER_STAGE_VERTEX_BIT,
                                       ctx.shaders["draw_billboard.vert"]);
        vku::add_pipeline_shader_stage(&pipelineInfo, VK_SHADER_STAGE_FRAGMENT_BIT,
                                       ctx.shaders["draw_billboard.frag"]);
        vku::get_default_pipeline_states(&pipelineInfo);
        pipelineInfo.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
        vku::create_graphics_pipeline(ctx.vtx, pipelineInfo, ctx.renderPasses["main"],
                                      ctx.setLayouts["draw_billboard"], 0,
                                      &ctx.pipelines["draw_billboard"]);
    }
}

void setup_vulkan_buffers(Context &ctx)
{
    // Create per-scene uniform buffers
    const VkBufferUsageFlags usage =
        (VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
    vku::create_buffer(ctx.vtx, 65536, usage, &ctx.buffers["scene_uniforms_0"]);
    vku::create_buffer(ctx.vtx, 65536, usage, &ctx.buffers["scene_uniforms_1"]);
}

void load_mesh(Context &ctx, const std::string &filename)
{
    // Load mesh data
    vku::OBJMesh mesh = {};
    vku::read_obj(filename.c_str(), &mesh);
    LOG_DEBUG("Vertex count: %d\n", uint32_t(mesh.positions.size() / 4));
    LOG_DEBUG("Index count: %d\n", uint32_t(mesh.indices.size()));
    ctx.mesh = mesh;

    // Create buffer object for vertex + index data
    const uint32_t numVertexBytes = uint32_t(mesh.positions.size() * sizeof(mesh.positions[0]));
    const uint32_t numIndexBytes = uint32_t(mesh.indices.size() * sizeof(mesh.indices[0]));
    const VkBufferUsageFlags usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT |
                                     VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
                                     VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    vku::create_buffer(ctx.vtx, numVertexBytes + numIndexBytes, usage, &ctx.buffers["mesh"]);

    // Upload data to buffer
    uint8_t *mappedData = nullptr;
    vmaMapMemory(ctx.vtx.allocator, ctx.buffers["mesh"].allocation, (void **)&mappedData);
    std::memcpy(mappedData, &mesh.positions[0], numVertexBytes);
    std::memcpy(mappedData + numVertexBytes, &mesh.indices[0], numIndexBytes);
    vmaUnmapMemory(ctx.vtx.allocator, ctx.buffers["mesh"].allocation);

    // Store info for vkBindVertexBuffers/vkBindIndexBuffer calls
    vku::VkuDrawInfo drawInfo = {};
    drawInfo.vertexBuffers = {ctx.buffers["mesh"].buffer};
    drawInfo.vertexOffsets = {0};
    drawInfo.indexBuffer = ctx.buffers["mesh"].buffer;
    drawInfo.indexOffset = numVertexBytes;
    drawInfo.indexType = VK_INDEX_TYPE_UINT32;
    drawInfo.count = uint32_t(mesh.indices.size());
    ctx.drawInfos["mesh"] = drawInfo;
}

void load_texture(Context &ctx, const std::string &filename)
{
    // Load RGBA image data from file
    int32_t width, height, comp;
    uint8_t *imageData = stbi_load(filename.c_str(), &width, &height, &comp, 4);
    LOG_DEBUG("Image dimensions: %dx%d (%d channels)\n", width, height, comp);

    // Create Vulkan objects (image + image view + sampler) for texture
    const VkExtent2D extent = {uint32_t(width), uint32_t(height)};
    const VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;
    const VkImageUsageFlags usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    vku::create_texture_2d(ctx.vtx, extent, format, usage, &ctx.textures["awesome"]);

    // Upload image data to texture
    const uint32_t numBytes = uint32_t(width * height * comp * sizeof(uint8_t));
    vku::update_texture_2d(&ctx.vtx, VK_NULL_HANDLE, ctx.textures["awesome"], extent, numBytes,
                           imageData);

    // Clean up
    stbi_image_free(imageData);
}

void transition_layouts_before_draw(Context &ctx)
{
    std::vector<VkImageMemoryBarrier> imageBarriers(2);
    vku::get_image_barrier(ctx.vtx.swapchain.colorImages[ctx.vtx.swapIndex],
                           VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                           &imageBarriers[0]);
    vku::get_image_barrier(ctx.vtx.swapchain.depthImages[ctx.vtx.swapIndex],
                           VK_IMAGE_LAYOUT_UNDEFINED,
                           VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, &imageBarriers[1]);
    imageBarriers[1].subresourceRange.aspectMask =
        VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
    vkCmdPipelineBarrier(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
                         VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0, 0, nullptr, 0, nullptr,
                         uint32_t(imageBarriers.size()), &imageBarriers[0]);
}

void transition_layouts_before_present(Context &ctx)
{
    VkImageMemoryBarrier imageBarrier;
    vku::get_image_barrier(ctx.vtx.swapchain.colorImages[ctx.vtx.swapIndex],
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                           VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, &imageBarrier);
    vkCmdPipelineBarrier(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
                         VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0, 0, nullptr, 0, nullptr, 1,
                         &imageBarrier);
}

void draw_scene(Context &ctx)
{
    // Update per-scene uniform buffers
    vkCmdUpdateBuffer(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex],
                      ctx.buffers["scene_uniforms_" + std::to_string(ctx.vtx.swapIndex)].buffer, 0,
                      uint32_t(sizeof(ctx.sceneUniforms)), &ctx.sceneUniforms);

    // Begin render pass instance
    VkRenderPassBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginInfo.renderPass = ctx.renderPasses["main"];
    beginInfo.framebuffer = ctx.framebuffers["main_" + std::to_string(ctx.vtx.swapIndex)];
    beginInfo.renderArea = {{0, 0}, {ctx.window.width, ctx.window.height}};
    vkCmdBeginRenderPass(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], &beginInfo,
                         VK_SUBPASS_CONTENTS_INLINE);

    // Clear color buffer
    const VkClearRect clearRect = {{{0, 0}, {ctx.window.width, ctx.window.height}}, 0, 1};
    const VkClearAttachment clearColor = {VK_IMAGE_ASPECT_COLOR_BIT, 0, {0.0f, 0.0f, 0.0f, 0.0f}};
    const VkClearAttachment clearDepth = {VK_IMAGE_ASPECT_DEPTH_BIT, 0, {1.0f, 0.0f}};
    vkCmdClearAttachments(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 1, &clearColor, 1, &clearRect);
    vkCmdClearAttachments(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 1, &clearDepth, 1, &clearRect);

    // Set dynamic pipeline state (viewport and scissor rectangles)
    const VkViewport viewport = {
        0, float(ctx.window.height), float(ctx.window.width), -float(ctx.window.height), 0, 1};
    const VkRect2D scissor = {{0, 0}, {ctx.window.width, ctx.window.height}};
    vkCmdSetViewport(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 0, 1, &viewport);
    vkCmdSetScissor(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 0, 1, &scissor);

#if SHOW_TRIANGLE_SCENE
    // Set up pipeline
    vkCmdBindPipeline(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], VK_PIPELINE_BIND_POINT_GRAPHICS,
                      ctx.pipelines["draw_triangle"].pipeline);

    // Draw triangle
    vkCmdDraw(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 3, 1, 0, 0);
#endif  // SHOW_TRIANGLE_SCENE

#if SHOW_MESH_SCENE
    // Set up pipeline
    vkCmdBindPipeline(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], VK_PIPELINE_BIND_POINT_GRAPHICS,
                      ctx.pipelines["draw_mesh"].pipeline);

    // Bind resources
    vku::VkuPushDescriptorInfo pdInfo = {};
    vku::add_descriptor_binding(&pdInfo, 0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, ctx.buffers["mesh"]);
    vku::add_descriptor_binding(&pdInfo, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                ctx.buffers["scene_uniforms_" + std::to_string(ctx.vtx.swapIndex)]);
    ctx.vtx.vkCmdPushDescriptorSetKHR(
        ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], VK_PIPELINE_BIND_POINT_GRAPHICS,
        ctx.pipelines["draw_mesh"].pipelineLayout, 0, uint32_t(pdInfo.descriptorWrites.size()),
        &pdInfo.descriptorWrites[0]);

    // Draw mesh
    const vku::VkuDrawInfo &drawInfo = ctx.drawInfos["mesh"];
    const uint32_t bindingCount = uint32_t(drawInfo.vertexOffsets.size());
    vkCmdBindVertexBuffers(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 0, bindingCount,
                           &drawInfo.vertexBuffers[0], &drawInfo.vertexOffsets[0]);
    vkCmdBindIndexBuffer(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], drawInfo.indexBuffer,
                         drawInfo.indexOffset, drawInfo.indexType);
    vkCmdDrawIndexed(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], drawInfo.count, 1, 0, 0, 0);
#endif  // SHOW_MESH_SCENE

#if SHOW_BILLBOARD_SCENE
    // Set up pipeline
    vkCmdBindPipeline(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], VK_PIPELINE_BIND_POINT_GRAPHICS,
                      ctx.pipelines["draw_billboard"].pipeline);

    // Bind resources
    vku::VkuPushDescriptorInfo pdInfo = {};
    vku::add_descriptor_binding(&pdInfo, 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                {ctx.textures["awesome"].sampler, ctx.textures["awesome"].imageView,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL});
    ctx.vtx.vkCmdPushDescriptorSetKHR(
        ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], VK_PIPELINE_BIND_POINT_GRAPHICS,
        ctx.pipelines["draw_billboard"].pipelineLayout, 0, uint32_t(pdInfo.descriptorWrites.size()),
        &pdInfo.descriptorWrites[0]);

    // Draw textured billboard
    vkCmdDraw(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 4, 1, 0, 0);
#endif  // SHOW_BILLBOARD_SCENE

    // End render pass instance
    vkCmdEndRenderPass(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex]);
}

void draw_imgui(Context &ctx)
{
    // Begin render pass instance
    VkRenderPassBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginInfo.renderPass = ctx.renderPasses["main"];
    beginInfo.framebuffer = ctx.framebuffers["main_" + std::to_string(ctx.vtx.swapIndex)];
    beginInfo.renderArea = {{0, 0}, {ctx.window.width, ctx.window.height}};
    vkCmdBeginRenderPass(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], &beginInfo,
                         VK_SUBPASS_CONTENTS_INLINE);

    // Draw ImGui
    ImGui::Render();
    ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), ctx.vtx.cmdBuffers[ctx.vtx.swapIndex]);

    // End render pass instance
    vkCmdEndRenderPass(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex]);
}

void do_initialization(Context &ctx)
{
    setup_vulkan_required_objects(ctx);
    setup_vulkan_command_buffers(ctx);
    setup_vulkan_render_passes(ctx);
    setup_vulkan_framebuffers(ctx);
    setup_vulkan_shaders(ctx);
    setup_vulkan_pipelines(ctx);
    setup_vulkan_buffers(ctx);

    load_mesh(ctx, shader_dir() + "../../data/bunny.obj");
    load_texture(ctx, shader_dir() + "../../data/awesome.png");
}

void do_update(Context &ctx)
{
    // Update per-scene uniforms
    const float aspect = float(ctx.window.width) / float(ctx.window.height);
    ctx.sceneUniforms.projFromView = glm::perspective(glm::radians(30.0f), aspect, 0.1f, 100.0f);
    ctx.sceneUniforms.viewFromWorld = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -3.5f));
    ctx.sceneUniforms.worldFromLocal =
        glm::translate(glm::mat4(1.0f), ctx.panning.translation) * glm::mat4(ctx.trackball.orient);
}

void do_rendering(Context &ctx)
{
    VkCommandBufferBeginInfo cmdBeginInfo = {};
    cmdBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmdBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkResetCommandBuffer(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], 0);
    vkBeginCommandBuffer(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex], &cmdBeginInfo);

    transition_layouts_before_draw(ctx);

    draw_scene(ctx);
    draw_imgui(ctx);

    transition_layouts_before_present(ctx);

    vkEndCommandBuffer(ctx.vtx.cmdBuffers[ctx.vtx.swapIndex]);

    VkSubmitInfo submitInfo = {};
    VkPipelineStageFlags waitStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &ctx.vtx.semaphores[0];
    submitInfo.pWaitDstStageMask = &waitStage;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &ctx.vtx.cmdBuffers[ctx.vtx.swapIndex];
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &ctx.vtx.semaphores[1];
    vkQueueSubmit(ctx.vtx.queue, 1, &submitInfo, VK_NULL_HANDLE);
}

void do_imgui(Context &ctx)
{
    ImGui::SetNextWindowCollapsed(true, ImGuiCond_Once);
    ImGui::ShowDemoWindow();

    // Update application's window title to show framerate
    char title[256] = {0};
    std::sprintf(title, "vktest (%.1f FPS)", ImGui::GetIO().Framerate);
    glfwSetWindowTitle(ctx.window.id, title);
}

void error_callback(int32_t /*error*/, const char *description)
{
    LOG_ERROR("%s\n", description);
}

void key_callback(GLFWwindow *window, int32_t key, int32_t scancode, int32_t action, int32_t mods)
{
    // Forward event to ImGui
    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) return;

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (key == GLFW_KEY_R && action == GLFW_PRESS) {
        setup_vulkan_shaders(*ctx);
        setup_vulkan_pipelines(*ctx);
    }
}

void char_callback(GLFWwindow *window, uint32_t codepoint)
{
    // Forward event to ImGui
    ImGui_ImplGlfw_CharCallback(window, codepoint);
    if (ImGui::GetIO().WantTextInput) return;
}

void mouse_button_callback(GLFWwindow *window, int32_t button, int32_t action, int32_t mods)
{
    // Forward event to ImGui
    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) return;

    double x, y;
    glfwGetCursorPos(window, &x, &y);

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        ctx->trackball.center = glm::vec2(x, y);
        ctx->trackball.tracking = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        ctx->panning.center = glm::vec2(x, y);
        ctx->panning.panning = (action == GLFW_PRESS);
    }
}

void cursor_pos_callback(GLFWwindow *window, double x, double y)
{
    // Forward event to ImGui
    if (ImGui::GetIO().WantCaptureMouse) return;

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    const glm::mat4 worldFromView = glm::inverse(ctx->sceneUniforms.viewFromWorld);
    cg::trackball_move(ctx->trackball, float(x), float(y), worldFromView);
    cg::panning_move_xy(ctx->panning, float(x), float(y), worldFromView);
}

void scroll_callback(GLFWwindow *window, double xOffset, double yOffset)
{
    // Forward event to ImGui
    ImGui_ImplGlfw_ScrollCallback(window, xOffset, yOffset);
    if (ImGui::GetIO().WantCaptureMouse) return;

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    const glm::mat4 worldFromView = glm::inverse(ctx->sceneUniforms.viewFromWorld);
    cg::panning_move_z(ctx->panning, float(yOffset), worldFromView);
}

void drop_callback(GLFWwindow *window, int32_t count, const char **paths)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (count > 0) {
        const std::string filename = std::string(paths[0]);
        if (filename.find(".obj") != std::string::npos) { load_mesh(*ctx, filename); }
        if (filename.find(".png") != std::string::npos) { load_texture(*ctx, filename); }
    }
}

void resize_callback(GLFWwindow *window, int32_t width, int32_t height)
{
    while (width == 0 && height == 0) {
        // Wait until window has been de-minimized, to avoid swapchain errors.
        // See https://vulkan-tutorial.com/Drawing_a_triangle/Swap_chain_recreation
        glfwGetFramebufferSize(window, &width, &height);
        glfwWaitEvents();
    }

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.width = width;
    ctx->window.height = height;

    // Re-create Vulkan swapchain and framebuffers
    vku::create_swapchain(&ctx->vtx);
    setup_vulkan_framebuffers(*ctx);
}

int main(int /*argc*/, char * /*argv*/ [])
{
    Context ctx = {};

    const char *env = std::getenv("VKTEST_SHOW_VULKAN_INFO");
    const bool showVulkanInfo = env ? std::atoi(env) : false;

    // Create window and Vulkan graphics context
    glfwSetErrorCallback(error_callback);
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    ctx.window.id = glfwCreateWindow(ctx.window.width, ctx.window.height, "vktest", 0, 0);
    vku::init_vulkan(&ctx.vtx, showVulkanInfo);
    glfwCreateWindowSurface(ctx.vtx.instance, ctx.window.id, nullptr, &ctx.vtx.surface);
    glfwSetWindowUserPointer(ctx.window.id, &ctx);
    glfwSetKeyCallback(ctx.window.id, key_callback);
    glfwSetCharCallback(ctx.window.id, char_callback);
    glfwSetMouseButtonCallback(ctx.window.id, mouse_button_callback);
    glfwSetCursorPosCallback(ctx.window.id, cursor_pos_callback);
    glfwSetScrollCallback(ctx.window.id, scroll_callback);
    glfwSetFramebufferSizeCallback(ctx.window.id, resize_callback);
    glfwSetDropCallback(ctx.window.id, drop_callback);

    // Load Vulkan extensions
    ctx.vtx.vkCmdPushDescriptorSetKHR = (PFN_vkCmdPushDescriptorSetKHR)vkGetDeviceProcAddr(
        ctx.vtx.device, "vkCmdPushDescriptorSetKHR");

    // Initialize application
    do_initialization(ctx);

    // Initialize ImGui
    vku::init_imgui(ctx.vtx, ctx.renderPasses["main"], ctx.window.id);

    // Start main loop
    while (!glfwWindowShouldClose(ctx.window.id)) {
        glfwPollEvents();
        ctx.window.elapsedTime = float(glfwGetTime());
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        do_update(ctx);
        do_imgui(ctx);
        do_rendering(ctx);
        vku::swap_buffers(&ctx.vtx);
    }

    // Shutdown everything
    vku::shutdown_imgui(ctx.vtx);
    vku::shutdown_vulkan(&ctx.vtx);
    glfwDestroyWindow(ctx.window.id);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
